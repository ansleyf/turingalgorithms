<?php
/**
    footer.php - Version 0.1.0
*/
?>
		<footer class="uk-width-1-1">
			<div class="ak">
				<div class="uk-text-center uk-text-small">
					&copy; 2014 <?php echo get_bloginfo( "name", "raw"); ?>. All Rights Reserved. &bull; Designed by <a href="http://www.comartlab.com/" target="_blank">The Commercial Art Lab</a>
				</div>
			</div>
		</footer>
	<?php wp_footer(); ?>
	</body>
</html>