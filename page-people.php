<?php
/**
    page-people.php - Version 0.1.0
*
*/
	get_header();
?>
	<div class="content uk-container uk-container-center">
	<?php 
		$args = array(
			"post_type" => "people",
			"nopaging" => true,
		);
		$the_query = new WP_Query($args);
		while ($the_query->have_posts()) : $the_query->the_post();
		$job_title = get_post_meta($post->ID, "job_title", true);
		$bio = get_post_meta($post->ID, "bio", true);
	?>
		<article class="uk-article person">
			<?php the_post_thumbnail("thumbnail", array("class" => "uk-align-medium-left")); ?>
			<h2><?php the_title(); if($job_title) echo ", " . $job_title; ?></h2>
			<?php echo $bio; ?>
		</article>
	<?php endwhile; ?>
	</div>
<?php
	get_footer();