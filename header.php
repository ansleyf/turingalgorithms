<?php
/**
    header.php - Version 0.1.0
*/
?>
<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title><?php wp_title( '|', true, 'right' ); ?><?php echo get_bloginfo( "name", "raw"); ?></title>

		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

		<?php wp_head(); ?>
		<link href="<?php echo get_theme_part(null, "style.css"); ?>" rel="stylesheet" type="text/css" />
        <script src="<?php echo get_theme_part("js", "base.js"); ?>"></script>
        <script src="<?php echo get_theme_part("js", "modernizer.js"); ?>"></script>

        <link rel="shortcut icon" href="/favicon.ico">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    </head>
    <body id="top" <?php body_class(); ?>>
		<?php
	        $menuargs = array(
	            "container"         => "",
	            "theme_location"    => "primary_nav",
	            "menu_class"        => "uk-navbar-nav uk-visible-large uk-float-right",
	            "menu_id"           => "",
	            'walker'            => new Walker_UIKIT
            );
            $offcanvnav = array(
	            "container"         => "",
	            "theme_location"    => "primary_nav",
	            "menu_class"        => "uk-nav uk-nav-offcanvas",
	            "menu_id"           => ""
        	);
        ?>
    <header class="uk-width-1-1">
	    	<nav class="uk-navbar">
	    		<div class="uk-container uk-container-center">
	    			<a href="<?php echo home_url(); ?>">
	    				<img class="logo" src="<?php echo get_theme_part("images", "logo.gif"); ?>" />
	    			</a>
	    			<?php wp_nav_menu($menuargs); ?>
	    			<a href="#offcanv" class="uk-navbar-toggle uk-float-right uk-hidden-large" data-uk-offcanvas></a>
	    		</div>
	    	</nav>
	    <div id="offcanv" class="uk-offcanvas">
			<div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
				<?php wp_nav_menu($offcanvnav); ?>
			</div>
		</div>
		
    </header>