<?php
	/**
		searchform.php - Version 0.1.0
	*/
?>

<form action="/" class="uk-search" method="get">
	<input type="text" class="uk-search-field uk-width-1-1" name="s" id="search" placeholder="SEARCH" value="<?php the_search_query(); ?>" />
</form>