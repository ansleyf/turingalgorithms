<?php
/**
    page-contact.php - Version 0.1.0
*
*/
	get_header();
?>
	<div class="content">
		<?php while (have_posts()) : the_post(); ?>
			<div class="uk-container uk-container-center">
				<article class="uk-article">
					<h2 class="uk-article-title"><?php the_title(); ?></h2>
					<?php the_content(); ?>
				</article>
				<div class="uk-grid uk-margin-top">
					<div class="uk-width-medium-3-10">
						<?php gravity_form(1, $false, false, false, null, true); ?>
					</div>
					<div class="uk-width-medium-7-10">
						<iframe class="uk-width-1-1" src="<?php echo get_post_meta($post->ID, "map_url", true); ?>" width="100%" height="300" frameborder="0" style="border:0"></iframe>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
<?php
	get_footer();