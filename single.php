<?php
/**
    single.php - Version 0.1.0
*/
	get_header();
?>
	<div class="content uk-container uk-container-center">
		<div class="uk-grid">
			<div class="uk-width-large-7-10">
			<?php while (have_posts()) : the_post(); ?>
				<article class="uk-article">
					<h2 class="uk-article-title uk-margin-bottom-remove"><?php the_title(); ?></h2>
					<div class="uk-article-meta uk-margin-bottom">Posted on <?php the_date(); ?> by <?php the_author(); ?> in <?php the_category(", "); ?></div>
					<?php the_content(); ?>
				</article>
			<?php endwhile; ?>
			</div>
			<div class="uk-width-3-10 uk-visible-large">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php
	get_footer();