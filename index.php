<?php 
/**
    index.php - Version 0.1.0
*/
	get_header(); 
?>
	<div class="content uk-container uk-container-center">
		<div class="uk-grid">
			<div class="uk-width-large-7-10">
			<?php while (have_posts()) : the_post(); ?>
				<?php get_template_part("loop", "view"); ?>
			<?php endwhile; ?>
			</div>
			<div class="uk-width-3-10 uk-visible-large">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php 
	get_footer();