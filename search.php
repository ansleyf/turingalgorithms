<?php
/**
    search.php - Version 0.1.0
*/
	get_header();
?>
    <div class="content uk-container uk-container-center">
        <h1>Search Results for: <?php echo get_search_query() ?></h1>
        <?php if ( have_posts() ) : while (have_posts()) : the_post(); ?>
                <?php get_template_part("loop","view"); ?>
            <?php endwhile; else : ?>
                <h1 class="uk-text-center">No Results Found!</h1>
                <?php get_search_form(); ?>
        <?php endif; ?>
        <ul class="uk-pagination uk-margin-top">
            <li class="uk-pagination-previous"><?php previous_posts_link("Newer Results"); ?></li>
            <li class="uk-pagination-next"><?php next_posts_link("Older Results"); ?></li>
        </ul>
    </div>
<?php
	get_footer();