<?php
/**
    page.php - Version 0.1.0
*/
	get_header();
?>
	<div class="content uk-container uk-container-center">
		<?php while (have_posts()) : the_post(); ?>
			<article class="uk-article">
				<h2 class="uk-article-title"><?php the_title(); ?></h2>
				<?php the_content(); ?>
			</article>
		<?php endwhile; ?>
	</div>
<?php
	get_footer();