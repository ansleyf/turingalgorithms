<?php
	get_header();
?>
	<?php 
		$args = array(
			"post_type" => "homepage_section",
			"nopaging" => true
		);
		$the_query = new WP_Query($args);
		while($the_query->have_posts()) : $the_query->the_post();
			$bg_type = get_post_meta($post->ID, "bg_type", true);
			$bg_image = get_post_meta($post->ID, "bg_image", true);
			if(!empty($bg_image)) $bg_image = $bg_image['guid'];
			if ($bg_type == "video") {
				$bg_video_mp4 = get_post_meta($post->ID, "bg_video_mp4", true);
				if(!empty($bg_video_mp4)) $bg_video_mp4 = $bg_video_mp4['guid'];
				$bg_video_webm = get_post_meta($post->ID, "bg_video_mp4", true);
				if(!empty($bg_video_webm)) $bg_video_webm = $bg_video_webm['guid'];
			}
			$button_link = get_post_meta($post->ID, "button_link", true);
			$button_text = get_post_meta($post->ID, "button_text", true);
			$below_button = get_post_meta($post->ID, "below_button", true);
			$dotnav[] = $post->post_name;
	?>
	<section id="<?php echo $post->post_name; ?>" class="front-page <?php echo $post->post_name; ?> bg-<?php echo $bg_type; ?>" style="background-image: url('<?php echo $bg_image; ?>');">
		<?php if($bg_type == "video") : ?>
			<video autoplay loop poster="<?php echo $bg_image; ?>" id="bgvid">
				<source src="<?php echo $bg_video_mp4; ?>" type="video/mp4">
				<source src="<?php echo $bg_video_webm; ?>" type="video/webm">
			</video>
		<?php endif; ?>
		<div class="uk-container uk-container-center uk-vertical-align uk-height-1-1">
			<div class="uk-vertical-align-middle">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
				<?php if($button_text && $button_link) : ?>
					<div class="uk-text-center uk-margin-large-top">
						<a class="cta" href="<?php echo $button_link; ?>"><?php echo $button_text; ?></a>
					</div>
				<?php endif; ?>
				<?php if($below_button) : ?>
					<div class="uk-text-center uk-margin-large-top">
						<?php echo $below_button; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<?php endwhile; ?>
	<div class="scroll-dots">
		<ul class="uk-dotnav uk-dotnav-vertical" data-uk-scrollspy-nav="{closest:'li', topoffset: -30, smoothscroll:true}">
			<?php foreach ($dotnav as $name) : ?>
				<li><a href="#<?php if($name === reset($dotnav)) { echo "top"; } else { echo $name; } ?>">&nbsp;</a></li>
			<?php endforeach; ?>
		</ul>
	</div>
<?php
	get_footer();